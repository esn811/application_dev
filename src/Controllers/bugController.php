<?php

namespace BugApp\Controllers;

use BugApp\Models\BugManager;
use BugApp\Models\Bug;
use BugApp\Controllers\abstractController;

class bugController extends abstractController
{

    public function show($id)
    {

        // Données issues du Modèle

        $manager = new BugManager();

        $bug = $manager->find($id);

        // Template issu de la Vue

        $content = $this->render('src/Views/Bug/'.$_SESSION['type'].'/show', ['bug' => $bug]);

        return $this->sendHttpResponse($content, 200);
    }

    public function index()
    {

        //$bugs = [];

        // TODO: liste des incidents
        $manager = new BugManager();
        $user = $_SESSION['user'];
        $type_user = $_SESSION['type'];

        switch($type_user){

            case 'engineer':
                $bugs = $manager->findAll();
                break;

            case 'recorder':
                $bugs = $manager->findAllRecorder($user->getId());
                break;

            default:
                die('You should not be there');

        }

        $content = $this->render('src/Views/Bug/'.$_SESSION['type'].'/list', ['bugs' => $bugs]);

        return $this->sendHttpResponse($content, 200);
    }

    public function add()
    {

        // Ajout d'un incident

        // TODO: ajout d'incident (GET et POST)
        $manager = new BugManager();
        if(isset($_POST['btn'])){
            $bug = new Bug();
            $bug->setTitle($_POST['titre']);
            $bug->setDescription($_POST['dscptn']);
            $bug->setCreatedAt($_POST['date'].' 00:00:00');
            $manager->add($bug,$_SESSION['user']);            
            header('Location:'.PUBLIC_PATH.'bug');
        }
        else{
        $content = $this->render('src/Views/Bug/Recorder/add', []);

        return $this->sendHttpResponse($content, 200);
        };
    }

    public function update($id_bug)
    {

        // Ajout d'un incident

        // TODO: ajout d'incident (GET et POST)
        $manager = new BugManager();
        $bug = $manager ->find($id_bug);

        if(isset($_POST['btn_up'])){
            if(isset($_POST['fin'])) {
                $manager->update($bug);            
            header('Location:'.PUBLIC_PATH.'bug/show/'.$id_bug);
            }
            else{
                header('Location:'.PUBLIC_PATH.'bug/update/'.$id_bug);
            }
            
        }
        else{
        $content = $this->render('src/Views/Bug/Engineer/update', ['bug' => $bug]);

        return $this->sendHttpResponse($content, 200);
        };
    }
     public function assignation_ajax($id_bug)
     {
        $manager = new BugManager();
        $bug = $manager ->find($id_bug);
        $manager->assignation_ajax($bug,$_SESSION['user']);
        echo json_encode(["nom"=>$_SESSION['user']->getNom()]);
        return;


     }

    public function cloture_ajax($id_bug)
    {
        $manager = new BugManager();
        $bug = $manager ->find($id_bug);
        $manager->update($bug);
        echo json_encode(["date"=>date("d/m/Y")]);
        return;


    }

    public function search_ajax($string,$id_filter)
    {
        $manager = new BugManager();
        $user = $_SESSION['user'];
        if ($id_filter=='assign') {
            $bugs=$manager->findAllEngineerAssigntome($user->getId(),$string);            
        }
        else if ($id_filter=='closed') {
            $bugs=$manager->findAllEngineerNotClosed($string);
        }
        else if ($id_filter=='lesdeux') {
            $bugs=$manager->findAllEngineerLesdeux($user->getId(),$string);
        }
        else if ($id_filter=='default') {
            $bugs=$manager->findAllsearch($string);

        }
        echo json_encode(["reponse"=>$bugs]);
            return;
    }



}
