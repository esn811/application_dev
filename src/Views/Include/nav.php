<body>
  <nav class="light-blue lighten-1" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="#" class="brand-logo">StarTickets!!</a>
      <ul class="right hide-on-med-and-down">
      <?php
                if(isset($_SESSION['user'])){

                    $user = $_SESSION['user'];

                ?>

                <li>                
                    <i class="material-icons">person</i>
                </li>
                <li>
                <span class="username"><?=$user->getNom();?>&nbsp</span>                
                </li>
                <li>
                    <a href="/ticketing/application-de-gestion-d-incidents/public/logout" class="user">
                        Logout
                    </a>
                </li>
                
            <?php
            }
            ?>
      </ul>

      <ul id="nav-mobile" class="sidenav">
        <li><a href="#">Navbar Link</a></li>
      </ul>
      <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    </div>
  </nav>
  <main> 
