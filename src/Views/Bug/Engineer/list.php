<?php
/** @var $bug \BugApp\Models\Bug */
$bugs = $parameters['bugs'];

?>

<!DOCTYPE html>

<html>

<head>
    <?php
    include('../src/Views/Include/header.php');

    ?>
</head>

<body>
    <?php include('../src/Views/Include/nav.php'); ?>
    <div class="section no-pad-bot" id="index-banner">
        <div class="container">
            <h2 class="header center orange-text">Liste des tickets</h2>
            <!-- Titre "filtes" -->
      <div class="row center">
        <div class="col s1">
          <h5 class="header center black-text">Filtres</h5>
        </div>
      </div>

      <!-- Création d'un checkbox -->
      <p>
        <label>
          <input type="checkbox" id="notclosed"/>
          <span>Afficher uniquement les incidents non clôturés</span>
        </label>
      </p>
      <!-- Fin création d'un checkbox -->
      <p>
        <label>
          <input type="checkbox" id="assigntome"/>
          <span>Afficher uniquement les incidents que je me suis assignés</span>
        </label>
      </p>
      <div class="row">
        <div class="input-field col s6">
          <i class="material-icons prefix">search</i>
          <input id="research" type="text" class="validate" placeholder="Saisir le sujet...">
        </div>
      <br></br>
            <table class="centered highlight responsive-table">
                <thead>
                    <tr>
                        <th width="14%">ID</th>
                        <th width="14%">Sujet</th>
                        <th width="14%" id="createdAt" >Date<i class="material-icons" id="test"></i></th>
                        <th width="14%">Utilisateur</th>
                        <th width="16%">Détail</th>
                        <th width="14%">Ingénieur</th>
                        <th width="14%">Clotûre</th>        
                    </tr>
                </thead>

                <tbody id="donnees">
                <?php foreach ($bugs as $test) {
                    echo ' <tr>
                    <td>'.$test->getId().'</td>
                    <td>'.$test->getTitle().'</td>
                    <td>'.$test->getCreatedAt()->format("d/m/Y").'</td>
                    <td>'.$test->getRecorder().'</td>
                    <td><a class="waves-effect waves-light btn-small" href="'.PUBLIC_PATH.'bug/show/'.$test->getId().'"><i class="material-icons left">subject</i>détails...</a></td>
                    ';
                    if($test->getEngineer() != null) {
                        echo '<td>'.$test->getEngineer().'</td>';
                    } else {
                        echo '<td><button class="waves-effect waves-light btn-small assigne_bug" value="'.$test->getId().'"  >Assigné</button></td>';
                    };
                    if($test->getClosedAt() != null) {
                        echo '<td>'.$test->getClosedAt()->format("d/m/Y").'</td>';
                    } elseif ($test->getEngineer() != null /* && id engineer == id user connecté */) {
                      echo '<td><button class="waves-effect waves-light btn-small cloture_bug" value="'.$test->getId().'"  >Cloturé</button></td>';
                    } else {
                      echo '<td>En cours</td>';
                    };
                }
                ?>
                
         
        </tbody>


            </table>
        </div>

    </div>
    </div>
    <br><br>


   
    <?php include('../src/Views/Include/footer.php') ?>

</body>

</html>