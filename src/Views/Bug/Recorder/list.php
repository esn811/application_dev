<?php
/** @var $bug \BugApp\Models\Bug */
$bugs = $parameters['bugs'];

?>

<!DOCTYPE html>

<html>

<head>
    <?php
    include('../src/Views/Include/header.php');

    ?>
</head>

<body>
    <?php include('../src/Views/Include/nav.php'); ?>
    <div class="section no-pad-bot" id="index-banner">
        <div class="container">
            <h2 class="header center orange-text">Liste de vos tickets</h2>
            <a class="btn-floating btn-large waves-effect waves-light turquoise" href="<?= PUBLIC_PATH; ?>bug/add"><i class="material-icons">add</i></a> &nbsp; Rapporter un incident <br><br>
            <table class="centered highlight responsive-table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Sujet</th>
                        <th>Date</th>
                        <th>Clotûre</th>
                        <th style="width: 15%;"></th>
                    </tr>
                </thead>

                <tbody>
                <?php foreach ($bugs as $test) {
                    echo ' <tr>
                    <td>'.$test->getId().'</td>
                    <td>'.$test->getTitle().'</td>
                    <td>'.$test->getCreatedAt()->format("d/m/Y").'</td>';
                    if($test->getClosedAt() != null) {
                        echo '<td>'.$test->getClosedAt()->format("d/m/Y").'</td>';
                    } else {
                        echo '<td></td>';
                    }
                    echo '
                        <td><a class="waves-effect waves-light btn-small" href="'.PUBLIC_PATH.'bug/show/'.$test->getId().'"><i class="material-icons left">subject</i>détails...</a></td>
                        </tr>
                    ';
                }
                ?>
                
         
        </tbody>


            </table>
        </div>

    </div>
    </div>
    <br><br>


   
    <?php include('../src/Views/Include/footer.php') ?>

</body>

</html>