<!DOCTYPE html>

<html>

<head>
    <?php
    include('../src/Views/Include/header.php');

    ?>
</head>

<body>
    <?php include('../src/Views/Include/nav.php'); ?>
    <main>
        <div class="container">
            <br><br>
            <a class="waves-effect waves-light btn-large" href="<?= PUBLIC_PATH; ?>bug"><i class="material-icons left">arrow_back</i>retour à la liste</a>
            <h3 class="header center teal-text lighten-1">Rapport d'incident</h3>
                <br><br>
                <div class="row center">
                    <div class="row">
                        <form method="POST" class="col s12">
                            <div class="row">
                                <div class="input-field col s6">
                                    <input placeholder="" id="titre" type="text" class="validate" name="titre">
                                    <label for="titre">Titre de l'incident :</label>
                                </div>
                                <div class="input-field col s6">
                                    <input id="date" type="date" class="validate" value="" name="date">
                                    <label for="date">Date de l'incident :</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <textarea id="description" class="materialize-textarea" placeholder="" name="dscptn"></textarea>
                                    <label for="description">Description de l'incident :</label>
                                </div>
                            </div>
                            <div class="row">
                            <button class="waves-effect waves-light btn" type="submit" name="btn"><i class="material-icons left">send</i>Soumettre</button>
                            </div>
                        </form>
                    </div>
                </div>
    </main>
    <!--  Scripts-->
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="/js/materialize.js"></script>
    <script src="/js/init.js"></script>
    <?php include('../src/Views/Include/footer.php'); ?>
</body>

</html>