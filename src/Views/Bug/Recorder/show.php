<?php

/** @var $bug \BugApp\Models\Bug */

$bug = $parameters['bug'];

?>

<!DOCTYPE html>

<html>

<head>
    <?php include('../src/Views/Include/header.php');    ?>
</head>

<body>
    <?php include('../src/Views/Include/nav.php'); ?>
    <div class="container">
        <br><br>
        <a class="waves-effect waves-light btn-large" href="<?= PUBLIC_PATH; ?>bug"><i class="material-icons left">arrow_back</i>retour à la liste</a>

        <h3 class="header center teal-text lighten-1">Fiche descriptive d'incidents</h3>
        <br>
        <div class="row">
            <div class="col l3">
                <h5 class="header teal-text">Nom de l'incident : </h5>
            </div>
            <div class="col l9">
                <h5 class="header teal-text text-darken-2"><?= $bug->getTitle(); ?></h5>
            </div>
        </div>
        <div class="row">
            <div class="col l3">
                <h5 class="header teal-text">Date d'observation :</h5>
            </div>
            <div class="col l9">
                <h5 class="header teal-text text-darken-2"><?php echo $bug->getCreatedAt()->format("d/m/Y"); ?></h5>
            </div>
        </div>
        <?php if ($bug->getClosedAt() != null) {
            echo '<div class="row">
        <div class="col l3">
          <h5 class="header teal-text">Date de clôture :</h5>
        </div>
        <div class="col l9">
          <h5 class="header teal-text text-darken-2">'.$bug->getClosedAt()->format("d/m/Y").'</h5>
        </div>
      </div>';
        }
        ?>

        <div class="row">
            <div class="col l3">
                <h5 class="header teal-text">Description de l'incident : </h5>
            </div>
            <div class="col l9">
                <p class="teal-text text-darken-2 justify"><?= $bug->getDescription(); ?></p>
            </div>
        </div>



    </div>
    <!-- <h2><?= $bug->getTitle(); ?></h2> 

    <div>Description : <?= $bug->getDescription(); ?></div>

   <div> Date de création : <?php echo $bug->getCreatedAt()->format("d/m/Y"); ?></div>

    <div>
    
        <?php if ($bug->getClosedAt() != null) {

            echo $bug->getClosedAt()->format("d/m/Y");
        }
        ?>
        
    </div>

-->
    <?php include('../src/Views/Include/footer.php') ?>

</body>

</html>