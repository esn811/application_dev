<?php

namespace BugApp\Models;

use BugApp\Services\Manager;
use DateTime;

class BugManager extends Manager
{
    public function findIdEngineer($id_user){
        $dbh = static::connectDb();

        $sth = $dbh->prepare('SELECT id FROM engineer WHERE engineer.user_id = :id_user');
        $sth->bindParam(':id_user', $id_user, \PDO::PARAM_INT);
        $sth->execute();
        $result = $sth->fetch(\PDO::FETCH_ASSOC);

        return $result['id'];
    }

    public function findIdRecorder($id_user){

        $dbh = static::connectDb();

        $sth = $dbh->prepare('SELECT id FROM recorder WHERE recorder.user_id = :id_user');
        $sth->bindParam(':id_user', $id_user, \PDO::PARAM_INT);
        $sth->execute();
        $result = $sth->fetch(\PDO::FETCH_ASSOC);

        return $result['id'];
    }
    public function find($id)
    {

        // Connexion à la BDD
        $dbh = static::connectDb();

        // Requête
        $sth = $dbh->prepare('SELECT * FROM bug WHERE id = :id');
        $sth->bindParam(':id', $id, \PDO::PARAM_INT);
        $sth->execute();
        $result = $sth->fetch(\PDO::FETCH_ASSOC);

        // Instanciation d'un bug
        $bug = new Bug();
        $bug->setId($result["id"]);
        $bug->setTitle($result["title"]);
        $bug->setDescription($result["description"]);
        $bug->setCreatedAt($result["createdAt"]);
        $bug->setClosedAt($result["closed"]);

        // Retour
        return $bug;
    }

    public function findAll()
    {

        // Récupération de tous les incidents en BDD
        $dbh = static::connectDb();

        // Requête
        $sth = $dbh->prepare('SELECT B.*,U.nom Nom_recorder,U_2.nom Nom_engineer
        FROM bug B 
        left join recorder R on B.recorder_id=R.id 
        left join user U on R.user_id=U.id
        left join engineer E on B.engineer_id=E.id 
        left join user U_2 on E.user_id=U_2.id    
        ORDER BY createdAt desc');
        $sth->execute();
        $bugs=[];
        while($result = $sth->fetch(\PDO::FETCH_ASSOC)){;

        // Instanciation d'un bug
        $bug = new Bug();
        $bug->setId($result["id"]);
        $bug->setTitle($result["title"]);
        $bug->setDescription($result["description"]);
        $bug->setCreatedAt($result["createdAt"]);
        $bug->setClosedAt($result["closed"]);
        $bug->setRecorder($result["Nom_recorder"]);
        $bug->setEngineer($result["Nom_engineer"]);
        array_push($bugs,$bug);
        };
         // Retour
         return $bugs;
    }

    public function findAllEngineerAssigntome($id_user,$string)
    {

        // Récupération de tous les incidents en BDD
        $dbh = static::connectDb();

        // Requête
        $sth = $dbh->prepare('SELECT B.id,B.title,B.description,U.nom Nom_recorder, B.createdAt,B.closed,U_2.nom Nom_engineer,engineer_id
        FROM bug B 
        left join recorder R on B.recorder_id=R.id 
        left join user U on R.user_id=U.id
        left join engineer E on B.engineer_id=E.id 
        left join user U_2 on E.user_id=U_2.id
        where U_2.id = ? and B.title like "%'.$string.'%"     
        ORDER BY createdAt desc');
        $sth->execute([$id_user]);
        $bugs=[];
        while($result = $sth->fetch(\PDO::FETCH_ASSOC)){;

        // Instanciation d'un bug
        $bug = new Bug();
        $bug->setId($result["id"]);
        $bug->setTitle($result["title"]);
        $bug->setDescription($result["description"]);
        $bug->setCreatedAt($result["createdAt"]);
        $bug->setClosedAt($result["closed"]);
        $bug->setRecorder($result["Nom_recorder"]);
        $bug->setEngineer($result["Nom_engineer"]);
        array_push($bugs,$bug);
        };
         // Retour
         return $bugs;
    }
    
    public function findAllEngineerNotClosed($string)
    {

        // Récupération de tous les incidents en BDD
        $dbh = static::connectDb();

        // Requête
        $sth = $dbh->prepare('SELECT B.id,B.title,B.description,U.nom Nom_recorder, B.createdAt,B.closed,U_2.nom Nom_engineer,engineer_id
        FROM bug B 
        left join recorder R on B.recorder_id=R.id 
        left join user U on R.user_id=U.id
        left join engineer E on B.engineer_id=E.id 
        left join user U_2 on E.user_id=U_2.id
        where closed is null and B.title like "%'.$string.'%"     
        ORDER BY createdAt desc');
        $sth->execute();
        $bugs=[];
        while($result = $sth->fetch(\PDO::FETCH_ASSOC)){;

        // Instanciation d'un bug
        $bug = new Bug();
        $bug->setId($result["id"]);
        $bug->setTitle($result["title"]);
        $bug->setDescription($result["description"]);
        $bug->setCreatedAt($result["createdAt"]);
        $bug->setClosedAt($result["closed"]);
        $bug->setRecorder($result["Nom_recorder"]);
        $bug->setEngineer($result["Nom_engineer"]);
        array_push($bugs,$bug);
        };
         // Retour
         return $bugs;
    }
    public function findAllEngineerLesdeux($id_user,$string)
    {

        // Récupération de tous les incidents en BDD
        $dbh = static::connectDb();

        // Requête
        $sth = $dbh->prepare('SELECT B.id,B.title,B.description,B.createdAt,B.closed,U_2.nom Nom_engineer,U.nom Nom_recorder
        FROM bug B 
        left join recorder R on B.recorder_id=R.id 
        left join user U on R.user_id=U.id
        left join engineer E on B.engineer_id=E.id 
        left join user U_2 on E.user_id=U_2.id
        where closed is null and U_2.id = ? and B.title like "%'.$string.'%"    
        ORDER BY createdAt desc');
        $sth->execute([$id_user]);
        $bugs=[];
        while($result = $sth->fetch(\PDO::FETCH_ASSOC)){;

        // Instanciation d'un bug
        $bug = new Bug();
        $bug->setId($result["id"]);
        $bug->setTitle($result["title"]);
        $bug->setDescription($result["description"]);
        $bug->setCreatedAt($result["createdAt"]);
        $bug->setClosedAt($result["closed"]);
        $bug->setRecorder($result["Nom_recorder"]);
        $bug->setEngineer($result["Nom_engineer"]);
        array_push($bugs,$bug);
        };
         // Retour
         return $bugs;
    }

    public function findAllsearch($string)
    {

        // Récupération de tous les incidents en BDD
        $dbh = static::connectDb();

        // Requête
        $sth = $dbh->prepare('SELECT B.id,B.title,B.description,U.nom Nom_recorder, B.createdAt,B.closed,U_2.nom Nom_engineer,engineer_id
        FROM bug B 
        left join recorder R on B.recorder_id=R.id 
        left join user U on R.user_id=U.id
        left join engineer E on B.engineer_id=E.id 
        left join user U_2 on E.user_id=U_2.id
        where B.title like "%'.$string.'%"     
        ORDER BY createdAt desc');
        $sth->execute([]);
        $bugs=[];
        while($result = $sth->fetch(\PDO::FETCH_ASSOC)){;

        // Instanciation d'un bug
        $bug = new Bug();
        $bug->setId($result["id"]);
        $bug->setTitle($result["title"]);
        $bug->setDescription($result["description"]);
        $bug->setCreatedAt($result["createdAt"]);
        $bug->setClosedAt($result["closed"]);
        $bug->setRecorder($result["Nom_recorder"]);
        $bug->setEngineer($result["Nom_engineer"]);
        array_push($bugs,$bug);
        };
         // Retour
         return $bugs;
    }

    public function findAllRecorder($id_user)
    {

        // Récupération de tous les incidents en BDD
        $dbh = static::connectDb();

        // Requête
        $sth = $dbh->prepare('SELECT bug.id, title, description,createdAt,closed
        FROM bug, recorder
        WHERE bug.recorder_id = recorder.id AND recorder.user_id = ?
        ORDER BY createdAt desc ');
        $sth->execute([$id_user]);
        $bugs=[];
        while($result = $sth->fetch(\PDO::FETCH_ASSOC)){;

        // Instanciation d'un bug
        $bug = new Bug();
        $bug->setId($result["id"]);
        $bug->setTitle($result["title"]);
        $bug->setDescription($result["description"]);
        $bug->setCreatedAt($result["createdAt"]);
        $bug->setClosedAt($result["closed"]);
        array_push($bugs,$bug);
        };
         // Retour
         return $bugs;
    }

    public function add(Bug $bug,$user){

        // Ajout d'un incident en BDD
        $bdd =static::connectDb();

        $req_insert=$bdd->prepare('insert into bug (title, description, createdAt, recorder_id) SELECT ?,?, ?,recorder.id FROM user,recorder where user.id=recorder.user_id AND recorder.user_id=?');
        $req_insert->execute([$bug->getTitle(), $bug->getDescription(), $bug->getCreatedAt()->format("Y-m-d H:i:s"), $user->getId()]);
        
    }

    public function update(Bug $bug){

        // Ajout d'un incident en BDD
        $bdd =static::connectDb();
 
        $req_insert=$bdd->prepare('update bug set closed= :closedate where id='.$bug->getID());
        $req_insert->execute(['closedate' => date("Y-m-d H:i:s")]);
 
     }
    
     public function assignation_ajax(Bug $bug,Engineer $user){

        // Ajout d'un incident en BDD
        $bdd =static::connectDb();
 
        $req_insert=$bdd->prepare('update bug set engineer_id= :engineer where id='.$bug->getID());
        $req_insert->execute(['engineer' => $this->findIdEngineer($user->getId())]);
 
     }

}
