let bug =document.querySelectorAll("#assigne_bug");
bug.forEach((element)=>{
    element.addEventListener("click",event => {
        event.preventDefault()
        assign(element.value, event)
    })

});
function assign(id,event) {

    let httpRequest = new XMLHttpRequest();
    httpRequest.open('GET', 'bug/Engineer/assignation/'+ id);
    httpRequest.send();

    httpRequest.onreadystatechange = () =>{
        if (httpRequest.readyState == XMLHttpRequest.DONE && httpRequest.status == 200) {
            let parsedData = JSON.parse(httpRequest.responseText);
            event.target.parentElement.innerHTML = parsedData.nom;
        }
    }
  };

let bug_2 =document.querySelectorAll("#cloture_bug");
bug_2.forEach((element)=>{
    element.addEventListener("click",event => {
        event.preventDefault()
        cloture(element.value, event)
    })

});
function cloture(id,event) {

    let httpRequest= new XMLHttpRequest();
    httpRequest.open('GET', 'bug/Engineer/cloture/'+ id);
    httpRequest.send();

    httpRequest.onreadystatechange = () =>{
        if (httpRequest.readyState == XMLHttpRequest.DONE && httpRequest.status == 200) {
            let parsedData = JSON.parse(httpRequest.responseText);
            event.target.parentElement.innerHTML = parsedData.date;
        }
    }
  };
  
let notclosed = document.getElementById('notclosed');
let assigntome = document.getElementById('assigntome');
notclosed.addEventListener('change',action)
assigntome.addEventListener('change',action);

function action(){
    let httpRequest= new XMLHttpRequest();
    
    let tot
    if (assigntome.checked && notclosed.checked) {
        tot='lesdeux';
        httpRequest.open('GET', 'bug/Engineer/filter/'+ tot);
        
    }
    else if (notclosed.checked) {
        tot='closed';
        httpRequest.open('GET', 'bug/Engineer/filter/'+ tot);

    }
    else if (assigntome.checked) {
        tot='assign';
        httpRequest.open('GET', 'bug/Engineer/filter/'+ tot);
    }
    else{
        tot='default'
        httpRequest.open('GET', 'bug/Engineer/filter/'+ tot);
    }
    httpRequest.send();
    httpRequest.onreadystatechange = () =>{
        if (httpRequest.readyState == XMLHttpRequest.DONE && httpRequest.status == 200) {
            let parsedData = JSON.parse(httpRequest.responseText);
            console.log(parsedData);
            let tableau='';
            let Public_Path='http://localhost:8000/ticketing/application-de-gestion-d-incidents/public/bug/show/';
            if (parsedData.reponse.length==0) {
                tableau = '<td colspan="7" style="text-align:center;vertical-align: middle;"> Désolé, aucun résultat ne correspond à votre demande</td>';
            }
            else{
                for (let i = 0; i < parsedData.reponse.length; i++) {
                    let engineer;
                    let closeddate;
                    console.log(parsedData.reponse)
                    if (parsedData.reponse[i].engineer != null) {
                        engineer=parsedData.reponse[i].engineer
                    }
                    else{
                        engineer='<button class="waves-effect waves-light btn-small" value="'+parsedData.reponse[i].id+'"  id="assigne_bug">Assigné</button>'
                    }
    
                    if(parsedData.reponse[i].closed) {
                        closeddate = parsedData.reponse[i].closed.date.split(' ')[0];
                    } 
                    else if (parsedData.reponse[i].engineer != null /* && id engineer == id user connecté */) {
                      closeddate = '<button class="waves-effect waves-light btn-small" value="'+parsedData.reponse[i].id+'"  id="cloture_bug">Cloturé</button>';
                    } 
                    else {
                      closeddate ='En cours';
                    };
                    tableau=tableau+`<tr>
                    <td>${parsedData.reponse[i].id}</td>
                    <td>${parsedData.reponse[i].title}</td>
                    <td>${parsedData.reponse[i].createdAt.date.split(' ')[0]}</td>
                    <td>${parsedData.reponse[i].recorder}</td>
                    <td><a class="waves-effect waves-light btn-small" href="${Public_Path+parsedData.reponse[i].id}"><i class="material-icons left">subject</i>détails...</a></td>
                    <td>${engineer}</td>
                    <td>${closeddate}</td>
                    </tr>`;
                    
                }
            }
            document.getElementById("donnees").innerHTML= tableau;
        }
    }
}

let research = document.getElementById('research')
research.addEventListener('input', search)
function search(){
    
    let httpRequest= new XMLHttpRequest();
    httpRequest.open('GET', 'bug/Engineer/search/'+ research.value);
    httpRequest.send();
    httpRequest.onreadystatechange = () =>{
        if (httpRequest.readyState == XMLHttpRequest.DONE && httpRequest.status == 200) {
            let parsedData = JSON.parse(httpRequest.responseText);
            console.log(parsedData);
            let tableau='';
            let Public_Path='http://localhost:8000/ticketing/application-de-gestion-d-incidents/public/bug/show/';
            if (parsedData.reponse.length==0) {
                tableau = '<td colspan="7" style="text-align:center;vertical-align: middle;"> Désolé, aucun résultat ne correspond à votre demande</td>';
            }
            else{
                for (let i = 0; i < parsedData.reponse.length; i++) {
                    let engineer;
                    let closeddate;
                    if (parsedData.reponse[i].engineer != null) {
                        engineer=parsedData.reponse[i].engineer
                    }
                    else{
                        engineer='<button class="waves-effect waves-light btn-small" value="'+parsedData.reponse[i].id+'"  id="assigne_bug">Assigné</button>'
                    }
    
                    if(parsedData.reponse[i].closed) {
                        closeddate = parsedData.reponse[i].closed.date.split(' ')[0];
                    } 
                    else if (parsedData.reponse[i].engineer != null /* && id engineer == id user connecté */) {
                      closeddate = '<button class="waves-effect waves-light btn-small" value="'+parsedData.reponse[i].id+'"  id="cloture_bug">Cloturé</button>';
                    } 
                    else {
                      closeddate ='En cours';
                    };
                    tableau=tableau+`<tr>
                    <td>${parsedData.reponse[i].id}</td>
                    <td>${parsedData.reponse[i].title}</td>
                    <td>${parsedData.reponse[i].createdAt.date.split(' ')[0]}</td>
                    <td>${parsedData.reponse[i].recorder}</td>
                    <td><a class="waves-effect waves-light btn-small" href="${Public_Path+parsedData.reponse[i].id}"><i class="material-icons left">subject</i>détails...</a></td>
                    <td>${engineer}</td>
                    <td>${closeddate}</td>
                    </tr>`;
                    
                }
            }
            document.getElementById("donnees").innerHTML= tableau;
        }
    }
}