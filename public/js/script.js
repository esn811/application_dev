let bug =document.querySelectorAll(".assigne_bug");
bug.forEach((element)=>{
    element.addEventListener("click",event => {
        event.preventDefault()
        assign(event)
    })

});
function assign(event) {

    let id= event.target.value;
    let httpRequest = new XMLHttpRequest();
    httpRequest.open('GET', 'bug/Engineer/assignation/'+ id);
    httpRequest.send();

    httpRequest.onreadystatechange = () =>{
        if (httpRequest.readyState == XMLHttpRequest.DONE && httpRequest.status == 200) {
            let parsedData = JSON.parse(httpRequest.responseText);
            event.target.parentElement.nextElementSibling.innerHTML = '<button class="waves-effect waves-light btn-small cloture_bug" value="'+event.target.value+'" >Clôturer</button>';
            test();
            event.target.parentElement.innerHTML = parsedData.nom;
        }
    }
  };
  test();
  function test(){
    let bug_2 =document.querySelectorAll(".cloture_bug");
    bug_2.forEach((element)=>{
      element.addEventListener("click",event => {
          event.preventDefault()
          cloture(event)
      })
  
  });
}

function cloture(event) {

    let id= event.target.value;
    let httpRequest= new XMLHttpRequest();
    httpRequest.open('GET', 'bug/Engineer/cloture/'+ id);
    httpRequest.send();

    httpRequest.onreadystatechange = () =>{
        if (httpRequest.readyState == XMLHttpRequest.DONE && httpRequest.status == 200) {
            let parsedData = JSON.parse(httpRequest.responseText);
            event.target.parentElement.innerHTML = parsedData.date;
        }
    }
  };

let notclosed = document.getElementById('notclosed');
let assigntome = document.getElementById('assigntome');
let research = document.getElementById('research')
notclosed.addEventListener('change',search)
assigntome.addEventListener('change',search);
research.addEventListener('input', search)

function search(){

    let httpRequest= new XMLHttpRequest();
    let tot
    if (assigntome.checked && notclosed.checked) {
        tot='lesdeux';
        httpRequest.open('GET', 'bug/Engineer/search/'+ research.value +'&'+ tot);
        
    }
    else if (notclosed.checked) {
        tot='closed';
        httpRequest.open('GET', 'bug/Engineer/search/'+ research.value +'&'+ tot);

    }
    else if (assigntome.checked) {
        tot='assign';
        httpRequest.open('GET', 'bug/Engineer/search/'+ research.value +'&'+ tot);
    }
    else{
        tot='default'
        httpRequest.open('GET', 'bug/Engineer/search/'+ research.value +'&'+ tot);
    }
    
    httpRequest.send();
    httpRequest.onreadystatechange = () =>{
        if (httpRequest.readyState == XMLHttpRequest.DONE && httpRequest.status == 200) {
            let parsedData = JSON.parse(httpRequest.responseText);
            console.log(parsedData);
            let tableau='';
            let Public_Path='http://localhost:8000/ticketing/application-de-gestion-d-incidents/public/bug/show/';
            if (parsedData.reponse.length==0) {
                tableau = '<td colspan="7" style="text-align:center;vertical-align: middle;"> Désolé, aucun résultat ne correspond à votre demande</td>';
            }
            else{
                for (let i = 0; i < parsedData.reponse.length; i++) {
                    let engineer;
                    let closeddate;
                    if (parsedData.reponse[i].engineer != null) {
                        engineer=parsedData.reponse[i].engineer
                    }
                    else{
                        engineer='<button class="waves-effect waves-light btn-small assigne_bug" value="'+parsedData.reponse[i].id+'" onclick="assign(event)">Assigné</button>'
                    }
    
                    if(parsedData.reponse[i].closed) {
                        closeddate = parsedData.reponse[i].closed.date.split(' ')[0];
                    } 
                    else if (parsedData.reponse[i].engineer != null /* && id engineer == id user connecté */) {
                      closeddate = '<button class="waves-effect waves-light btn-small" value="'+parsedData.reponse[i].id+'" onclick="cloture(event)"  >Cloturé</button>';
                    } 
                    else {
                      closeddate ='En cours';
                    };
                    tableau=tableau+`<tr>
                    <td>${parsedData.reponse[i].id}</td>
                    <td>${parsedData.reponse[i].title}</td>
                    <td>${parsedData.reponse[i].createdAt.date.split(' ')[0]}</td>
                    <td>${parsedData.reponse[i].recorder}</td>
                    <td><a class="waves-effect waves-light btn-small" href="${Public_Path+parsedData.reponse[i].id}"><i class="material-icons left">subject</i>détails...</a></td>
                    <td>${engineer}</td>
                    <td>${closeddate}</td>
                    </tr>`;
                    
                }
            }
            document.getElementById("donnees").innerHTML= tableau;
        }
    }
}

