<?php

require('../src/init.php');

session_start();

use BugApp\Controllers\bugController;
use BugApp\Controllers\UserController;

switch(true) {

    case preg_match('#^bug/show/(\d+)$#', $uri, $matches):

        $id = $matches[1];

        $controller = new bugController();

        return $controller->show($id);

        break;

    case preg_match('#^bug/update/(\d+)$#', $uri, $matches):

        $id = $matches[1];
    
        $controller = new bugController();
    
        return $controller->update($id);
    
        break;
    
    case preg_match('#^bug((\?)|$)#', $uri):

        $controller = new bugController();

        return $controller->index();

        break;

    case preg_match('#^bug/Engineer/assignation/(\d+)$#', $uri,$matches):

        $id = $matches[1];

        $controller = new bugController();
    
        return $controller->assignation_ajax($id);
    
        break;

    case preg_match('#^bug/Engineer/cloture/(\d+)$#', $uri,$matches):

        $id = $matches[1];
    
        $controller = new bugController();
        
        return $controller->cloture_ajax($id);
        
        break;

    case preg_match('#^bug/Engineer/search/(\w*)&(\w+)$#', $uri,$matches):

        $string = $matches[1];
        
        $id_filter = $matches[2];

        $controller = new bugController();
                
        return $controller->search_ajax($string,$id_filter);
                
        break;


    case ($uri == 'bug/add'):

        $controller = new bugController();
        
        return $controller->add();
        
        break;
    
    case ($uri == 'login'):

        $controller = new UserController();
            
        return $controller->login();
            
        break;
    
    case ($uri == 'logout'):

        $controller = new UserController();
            
        return $controller->logout();
            
        break;
     
    default:
    
    http_response_code(404);
    
    echo "<h1>Gestion d'incidents</h1><p>Page par défaut</p>";
    
}